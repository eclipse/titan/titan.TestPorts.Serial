# titan.TestPorts.Serial

## Eclipse Titan project website

* https://projects.eclipse.org/projects/tools.titan

## Source code of the TTCN-3 compiler and executor

* https://gitlab.eclipse.org/eclipse/titan/titan.core

## Contacts
If you found a bug or have a feature request, contact the project developers:
* https://projects.eclipse.org/projects/tools.titan/contact

## Usage

- clone this repo to your computer in a folder (home_folder from now on)

- Create a "bin" directory and cd into it

```
$ mkdir bin
$ cd ./bin
```

- Create symlinks to all files in src and demo directory here

```
$ ln -s ../src/* .
$ ln -s ../demo/* .
```

- Create the makefile

```
$ makefilegen -e "SerialPortTests" ./*
```

- Compile the project

```
$ make
```

- Adjust serial port properties in SerialPort.cfg

- Run the tests

```
$ ttcn3_start SerialPortTests SerialPort.cfg
```
